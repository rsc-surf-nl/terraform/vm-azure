provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }

  subscription_id            = var.azure_subscription_id
  client_id                  = var.azure_client_id
  client_secret              = var.azure_client_secret
  tenant_id                  = var.azure_tenant_id
  skip_provider_registration = true
}

provider "azapi" {
  subscription_id  = var.azure_subscription_id
  client_id        = var.azure_client_id
  client_secret    = var.azure_client_secret
  tenant_id        = var.azure_tenant_id
  default_tags     = local.tag_map
  default_location = var.azure_location
}
