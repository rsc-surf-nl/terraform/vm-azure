# Resource/flavour specific parameters

variable "azure_os_type" {
  type        = string
  description = "[flavour] The OS type. Should be either `linux` or `windows`."
  validation {
    condition     = contains(["linux", "windows"], var.azure_os_type)
    error_message = "The `azure_os_type` should be either 'linux' or 'windows'."
  }
}

variable "azure_vm_size" {
  type        = string
  description = "[flavour] The Azure VM size (eg. 'Standard_D2s_v3')"
}

variable "azure_os_image_subscription_id" {
  type        = string
  description = "[flavour] The Azure subscription under which the shared OS image is stored"
}

variable "azure_os_image_resource_group" {
  type        = string
  description = "[flavour] The resource group in which the OS images are stored"
}

variable "azure_os_image_gallery" {
  type        = string
  description = "[flavour] The name of the image gallery in which the OS images are stored"
}

variable "azure_os_image_name" {
  type        = string
  description = "[flavour] The image name to find the OS image"
}


variable "boot_volume_size" {
  type        = number
  description = "[flavour] The size (in GB) of the root partition Linux or the C drive on Windows"
  default     = 30
}

variable "min_os_disk_size" {
  type    = number
  default = 30
}

# Optional additional resources
variable "quantum_workspace" {
  type        = bool
  description = "Will create an Azure Quantum Workspace and associated resources"
  default     = false
}

# Research Cloud specific variables

variable "instance_tag" {
  type        = string
  description = "Value for the tag 'instance_tag' on all Azure resources"
}

variable "cidr" {
  type        = string
  description = "The CIDR network space to use for the local (Azure) network of the VM"
  default     = "10.0.0.0/16"
  validation {
    condition     = length(regexall("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})$", var.cidr)) > 0
    error_message = "The `cidr` should be a valid CIDR network space in a notation like '10.0.0.0/16'."
  }
}

variable "custom_tags" {
  description = "Additional tags to attach to the resources"
  type        = map(any)
  default     = {}
}

variable "instance_user" {
  type        = string
  description = "Username for the primary/root user"
}

variable "security_group_rules" {
  type        = list(string)
  description = "A list of security group rules with highest precendence. Rule syntax: {direction} {tcp|udp} {port range start} {port range end} {source IP/CIDR network}"
  default     = []
}

variable "config_security_group_rules" {
  type        = list(string)
  description = "A list of security group rules in addition to the ones defined by the `security_group_rules` parameter. They have intermediate precendence. Rule syntax: {direction} {tcp|udp} {port range start} {port range end} {source IP/CIDR network}"
  default     = []
}

variable "external_volumes" {
  type        = string
  description = "A JSON encoded list of managed volumes that should be attached to this VM. The datastructure of each entry is determined by the output of the `volume-azure` Terraform script. They should be stored within the same Azure subscription."
  default     = "[]"
}

variable "external_floating_ips" {
  type        = string
  description = "A JSON encoded list of the (floating) public IP addresses that should be attached to this VM. The datastructure of each entry is determined by the output of the `ip-azure` Terraform script. They should be stored within the same Azure subscription. Note that only the first entry will be used, the rest will silently be ignored."
  default     = "[]"
}

variable "external_networks" {
  type        = string
  description = "A JSON encoded list of networks that should be attached to this VM. The datastructure of each entry is determined by the output of the `network-azure` Terraform script. They should be stored within the same Azure subscription. Note that only the first entry will be used, the rest will silently be ignored."
  default     = "[]"
}

# Azure subscription specific variables

variable "azure_subscription_id" {
  type        = string
  description = "[subscription] The Azure subscription under which to start this VM"
}

variable "azure_client_id" {
  type        = string
  description = "[subscription] The client ID that should be used for provisioning the resources"
}

variable "azure_client_secret" {
  type        = string
  description = "[subscription] The client secret that should be used for provisioning the resources"
}

variable "azure_tenant_id" {
  type        = string
  description = "[subscription] The ID of the tenant to which the client ID belongs"
}

variable "azure_location" {
  type        = string
  description = "[subscription] The Azure location where to deploy the VM"
  default     = "West Europe"
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Compute"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "Azure"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "VM"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
