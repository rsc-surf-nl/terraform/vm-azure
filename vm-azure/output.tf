output "vm_id" {
  description = "The ID of the VM in the cloud environment"
  value       = var.azure_os_type == "windows" ? azurerm_windows_virtual_machine.azure_single_vm[0].id : azurerm_linux_virtual_machine.azure_single_vm[0].id
}

output "id" {
  description = "The ID of the VM in the cloud environment"
  value       = var.azure_os_type == "windows" ? azurerm_windows_virtual_machine.azure_single_vm[0].id : azurerm_linux_virtual_machine.azure_single_vm[0].id
}

output "vm_name" {
  description = "The name of the VM"
  value       = local.vm_reference.name
}

output "vm_resource_group" {
  description = "The name of the VM"
  value       = azurerm_resource_group.azure_resource_group.name
}

output "ip" {
  description = "The public IP address of the VM"
  value       = local.dns_ipv4
}

output "local_ip" {
  value = local.external_network != null ? azurerm_network_interface.nic.private_ip_address : null
}

output "instance_user" {
  description = "The username of the admin user account"
  value       = var.instance_user
}

output "min_os_disk_size" {
  value = var.min_os_disk_size
}

output "instance_password" {
  description = "The password for the admin user"
  value       = local.admin_password
  sensitive   = true
}

output "private_key" {
  description = "The private key that should be used to SSH to this machine"
  value       = tls_private_key.ssh_key.private_key_pem
  sensitive   = true
}

output "public_key" {
  description = "The public SSH key. This is already installed on the machine."
  value       = tls_private_key.ssh_key.public_key_openssh
}

output "identity" {
  value = local.vm_reference.identity[0]
}

output "network_secgroup_name" {
  value = azurerm_network_security_group.nsg.name
}

# Quantum specific outputs
output "quantum_workspace_id" {
  description = "The ID of the Quantum workspace in the cloud environment"
  value       = var.quantum_workspace ? azapi_resource.quantum_workspace[0].id : ""
}

output "quantum_workspace_location" {
  description = "The Azure location of the Quantum Workspace"
  value       = var.quantum_workspace ? var.azure_location : ""
}

# Mark support for updating vm NSGs
output "terraform_script_version" {
  value = 5.1
}

output "flavor_name" {
  value = var.azure_vm_size
}
