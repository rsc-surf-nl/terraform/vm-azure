resource "azurerm_resource_group" "quantum_resource_group" {
  count    = var.quantum_workspace ? 1 : 0
  name     = "src-${var.workspace_id}-quantum"
  location = var.azure_location
  tags     = local.tag_map
}

resource "azurerm_storage_account" "quantum_storage" {
  count = var.quantum_workspace ? 1 : 0
  # This name is not completely safe (ie. unique), but I guess safe enough
  name                     = substr(replace(var.workspace_id, "-", ""), 0, 24)
  resource_group_name      = azurerm_resource_group.quantum_resource_group[0].name
  location                 = azurerm_resource_group.quantum_resource_group[0].location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags                     = local.tag_map
}

resource "azapi_resource" "quantum_workspace" {
  count     = var.quantum_workspace ? 1 : 0
  type      = "Microsoft.Quantum/workspaces@2022-01-10-preview"
  name      = "src-${var.workspace_id}"
  parent_id = azurerm_resource_group.quantum_resource_group[0].id
  identity {
    type = "SystemAssigned"
  }
  body = jsonencode(
    {
      properties = {
        providers = [
          {
            applicationName = "src-${var.workspace_id}-ionq"
            providerId      = "ionq"
            providerSku     = "pay-as-you-go-cred"
          },
          {
            applicationName = "src-${var.workspace_id}-quantinuum"
            providerId      = "quantinuum"
            providerSku     = "credits1"
          },
          {
            applicationName = "src-${var.workspace_id}-rigetti"
            providerId      = "rigetti"
            providerSku     = "azure-quantum-credits"
          },
          {
            applicationName = "src-${var.workspace_id}-Microsoft"
            providerId      = "Microsoft"
            providerSku     = "DZH3178M639F"
          }
        ]
        storageAccount = azurerm_storage_account.quantum_storage[0].id
      }
    }
  )
  tags = local.tag_map
}

resource "azurerm_role_assignment" "storage_access_for_quantum" {
  count                            = var.quantum_workspace ? 1 : 0
  scope                            = azurerm_storage_account.quantum_storage[0].id
  principal_id                     = azapi_resource.quantum_workspace[0].identity[0].principal_id
  role_definition_name             = "Contributor"
  skip_service_principal_aad_check = true
}

resource "azurerm_role_assignment" "quantum_access_for_vm" {
  count                            = var.quantum_workspace ? 1 : 0
  scope                            = azapi_resource.quantum_workspace[0].id
  principal_id                     = local.vm_reference.identity[0].principal_id
  role_definition_name             = "Contributor"
  skip_service_principal_aad_check = true
}
