resource "azurerm_virtual_network" "avn" {
  count               = local.external_network != null ? 0 : 1 # Only create a network if no explicit external network has been assigned
  name                = "${azurerm_resource_group.azure_resource_group.name}_avn"
  address_space       = [var.cidr]
  location            = azurerm_resource_group.azure_resource_group.location
  resource_group_name = azurerm_resource_group.azure_resource_group.name
  tags                = local.tag_map
}

resource "azurerm_subnet" "subnet" {
  count                = local.external_network != null ? 0 : 1 # Only create a network if no explicit external network has been assigned
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.azure_resource_group.name
  virtual_network_name = azurerm_virtual_network.avn[0].name
  address_prefixes     = [var.cidr]
}

resource "azurerm_network_security_group" "nsg" {
  name                = "${azurerm_resource_group.azure_resource_group.name}_nsg"
  location            = azurerm_resource_group.azure_resource_group.location
  resource_group_name = azurerm_resource_group.azure_resource_group.name
  tags                = local.tag_map
}

# Note that there is no need to add rules to allow traffic from the virtual network. Azure allows all traffic from the internal network by default!
resource "azurerm_network_security_rule" "open_ports" {
  count                      = length(local.security_group_rules)
  name                       = replace(replace(local.security_group_rules[count.index], "/", "_"), " ", "_")
  direction                  = lower(split(" ", local.security_group_rules[count.index])[0]) == "in" ? "Inbound" : "Outbound"
  access                     = "Allow"
  priority                   = 100 + count.index
  source_address_prefix      = split(" ", local.security_group_rules[count.index])[4]
  source_port_range          = "*"
  destination_address_prefix = "*"
  destination_port_range = format(
    "%d-%d",
    split(" ", local.security_group_rules[count.index])[2],
    split(" ", local.security_group_rules[count.index])[3],
  )
  protocol                    = lower(split(" ", local.security_group_rules[count.index])[1]) == "udp" ? "Udp" : "Tcp"
  resource_group_name         = azurerm_resource_group.azure_resource_group.name
  network_security_group_name = azurerm_network_security_group.nsg.name
}

resource "azurerm_public_ip" "pubip" {
  count                   = local.external_floating_ip != null ? 0 : 1 # Only create an IP if no explicit external IP has been assigned
  name                    = "${azurerm_resource_group.azure_resource_group.name}_pubip"
  location                = azurerm_resource_group.azure_resource_group.location
  resource_group_name     = azurerm_resource_group.azure_resource_group.name
  allocation_method       = "Static"
  idle_timeout_in_minutes = 30
  tags                    = local.tag_map
}

resource "azurerm_network_interface" "nic" {
  name                = "${azurerm_resource_group.azure_resource_group.name}_nic"
  location            = azurerm_resource_group.azure_resource_group.location
  resource_group_name = azurerm_resource_group.azure_resource_group.name
  tags                = local.tag_map

  ip_configuration {
    name                          = "internal"
    subnet_id                     = local.external_network != null ? local.external_network.subnet_id.value : azurerm_subnet.subnet[0].id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = local.external_floating_ip != null ? local.external_floating_ip.id.value : azurerm_public_ip.pubip[0].id
  }
}

resource "time_sleep" "wait_befor_nic_removal" {
  depends_on = [azurerm_network_interface.nic]

  destroy_duration = "181s"
}

resource "azurerm_network_interface_security_group_association" "sg_association" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
  # I believe the next explicit dependency is needed to make sure the security group is fully
  # configured before being used. Although this is actually more important when destroying
  # resources: this fails frequently.
  depends_on = [
    azurerm_network_security_rule.open_ports,
  ]
}
