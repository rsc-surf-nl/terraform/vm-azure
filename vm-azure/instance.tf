locals {
  tag_map = merge(
    var.custom_tags,
    {
      application_type   = var.application_type
      cloud_type         = var.cloud_type
      co_id              = var.co_id
      instance_name      = var.workspace_fqdn
      instance_tag       = var.instance_tag
      resource_type      = var.resource_type
      subscription       = var.subscription
      subscription_group = var.subscription_group
      wallet_id          = var.wallet_id
      workspace_id       = var.workspace_id
    }
  )
  vm_reference = var.azure_os_type == "windows" ? azurerm_windows_virtual_machine.azure_single_vm[0] : azurerm_linux_virtual_machine.azure_single_vm[0]
  security_group_rules = distinct(concat(
    [for item in var.security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")],
    [for item in var.config_security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")]
  ))
  external_volumes     = tolist(jsondecode(var.external_volumes))
  external_floating_ip = length(tolist(jsondecode(var.external_floating_ips))) != 0 ? tolist(jsondecode(var.external_floating_ips))[0] : null
  external_network     = length(tolist(jsondecode(var.external_networks))) != 0 ? tolist(jsondecode(var.external_networks))[0] : null
  dns_ipv4             = local.external_floating_ip != null ? local.external_floating_ip.address.value : azurerm_public_ip.pubip[0].ip_address
  admin_password       = "Ab1&${random_password.admin_password.result}"
}

resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "random_password" "admin_password" {
  length           = 24
  special          = true
  override_special = "&"
}

# Create a resource group
resource "azurerm_resource_group" "azure_resource_group" {
  name     = "src-${var.workspace_id}"
  location = var.azure_location
  tags     = local.tag_map
}

resource "azurerm_windows_virtual_machine" "azure_single_vm" {
  count                 = var.azure_os_type == "windows" ? 1 : 0
  name                  = var.workspace_fqdn
  location              = azurerm_resource_group.azure_resource_group.location
  resource_group_name   = azurerm_resource_group.azure_resource_group.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  size                  = var.azure_vm_size
  tags                  = local.tag_map

  #MB -> use our own license (cheaper VMs):
  license_type = "Windows_Server"

  source_image_id = "/subscriptions/${var.azure_os_image_subscription_id}/resourceGroups/${var.azure_os_image_resource_group}/providers/Microsoft.Compute/galleries/${var.azure_os_image_gallery}/images/${var.azure_os_image_name}"

  os_disk {
    name                 = "${azurerm_resource_group.azure_resource_group.name}_osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = max(var.boot_volume_size, var.min_os_disk_size)
  }

  admin_username     = var.instance_user
  admin_password     = local.admin_password
  computer_name      = split(".", var.workspace_fqdn)[0]
  custom_data        = filebase64("${path.module}/files/ansible-winrm-config.ps1")
  provision_vm_agent = true

  winrm_listener {
    protocol = "Http"
  }

  additional_unattend_content {
    setting = "AutoLogon"
    content = "<AutoLogon><Password><Value>${replace(replace(replace(replace(replace(local.admin_password, "\"", "&quot;"), "'", "&apos;"), "<", "&lt;"), ">", "&gt;"), "&", "&amp;")}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${replace(replace(replace(replace(replace(var.instance_user, "\"", "&quot;"), "'", "&apos;"), "<", "&lt;"), ">", "&gt;"), "&", "&amp;")}</Username></AutoLogon>"
  }

  additional_unattend_content {
    setting = "FirstLogonCommands"
    content = file("${path.module}/files/tpl.first_logon_commands.xml")
  }

  identity {
    type = "SystemAssigned"
  }

  depends_on = [azurerm_network_interface_security_group_association.sg_association]
}

resource "azurerm_linux_virtual_machine" "azure_single_vm" {
  count                           = var.azure_os_type == "linux" ? 1 : 0
  name                            = var.workspace_fqdn
  location                        = azurerm_resource_group.azure_resource_group.location
  resource_group_name             = azurerm_resource_group.azure_resource_group.name
  network_interface_ids           = [azurerm_network_interface.nic.id]
  size                            = var.azure_vm_size
  computer_name                   = var.workspace_fqdn
  admin_username                  = var.instance_user
  disable_password_authentication = true
  tags                            = local.tag_map

  admin_ssh_key {
    username   = var.instance_user
    public_key = tls_private_key.ssh_key.public_key_openssh
  }

  source_image_id = "/subscriptions/${var.azure_os_image_subscription_id}/resourceGroups/${var.azure_os_image_resource_group}/providers/Microsoft.Compute/galleries/${var.azure_os_image_gallery}/images/${var.azure_os_image_name}"

  os_disk {
    name                 = "${azurerm_resource_group.azure_resource_group.name}_osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = max(var.boot_volume_size, var.min_os_disk_size)
  }

  identity {
    type = "SystemAssigned"
  }

  depends_on = [azurerm_network_interface_security_group_association.sg_association]
}

resource "azurerm_virtual_machine_data_disk_attachment" "external_volume_attachments" {
  count              = length(local.external_volumes)
  managed_disk_id    = local.external_volumes[count.index].id.value
  virtual_machine_id = local.vm_reference.id
  lun                = local.external_volumes[count.index].index.value + 10
  caching            = "ReadWrite"
}
